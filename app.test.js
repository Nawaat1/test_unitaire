const request = require('supertest')
const app = require('./app')


describe('Cat Api', () => {

    it("GET /cats récupération d'un tableau de chat", ()=> {
        return request(app).get('/cats').expect('Content-type', /json/)
        .expect(200)
        .then((res) =>{
            expect(res.body).toEqual(
                expect.arrayContaining([
                    expect.objectContaining({
                    id: expect.any(Number),
                    name: expect.any(String)
                }),
                ])
            )
        })
    });

    it("GET /cat/:id récupération d'un chat", ()=>{
        return request(app).get('/cats/2')
        .expect('Content-type', /json/)
        .expect(200)
        .then((res) =>{
            expect(res.body).toEqual(
                    expect.objectContaining({
                    id: expect.any(Number),
                    name: expect.any(String)
                }),
            )
        })
    })


    it("GET /cat/:id echec de la récupération d'un chat retourne une erreur 404", ()=> {
        return request(app).get("/212").expect(404);
    })



    it("POST /cats creation d'un chat", ()=> {
        return request(app).post("/cats").send({
            id: 5,
            name: "lechat"
        })
        .expect("Content-Type", /json/)
        .expect(201).then(()=>{
            expect.objectContaining({
                id: 5,
                name:'lechat',
            })
        })
    })

    
    it("PUT /:id modifier le chat", ()=> {
        return request(app).put("/cats/1").send({
            id: 1,
            name: "biche"
        })
    })


    it("Delete /:id supprimer le chat", ()=> {
        return request(app).delete("/cats/2")
        .expect("Content-Type", /json/)
        .expect(200)
        })
    

})