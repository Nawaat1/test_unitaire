var express = require('express');
var router = express.Router();
const CreateError = require('http-errors')

const cats = [

  {
    id: 1,
    name: 'Simba',
  },
  {
    id: 2,
    name: 'loulou',
  },
  {
    id: 2,
    name: 'loulou',
  }
]


/* GET cats listing. */
router.get('/', function(req, res, next) {

  res.json(cats)
});

//create new cat
router.post('/', function(req, res, next) {

  const {body} = req
  const newCat = {
    id: cats.lenght + 1,
    name: body.name,
  };

  cats.push(newCat);
  res.status(201).json(newCat);

});


//Get one cat
router.get("/:id", function(req, res, next){

  const cat = cats.find(cat => cat.id === Number(req.params.id));
  if(!cat){
    return next(CreateError(404, "Le chat n'existe pas"))
  }
  res.json(cat);

})

//
router.put('/:id', (req, res) => {

  const index = cats.findIndex(index => index.id === Number(req.params.id));

   if(!index){
      return next(CreateError(404, "Le chat n'existe pas"))
  } 
      cats[index].name = req.body.name;
      res.json(cats[index]);

})


router.delete('/:id', (req, res,next) => {
  const index = cats.findIndex(index => index.id === Number(req.params.id));

  if(!index){
    return next(CreateError(404, "Le chat n'existe pas"))
  }
  cats.splice(index, 1);
  res.status(200).json('le chat a bien été supprimer')
})



module.exports = router;
